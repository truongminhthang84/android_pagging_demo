package com.example.androidpaging.db

import androidx.paging.DataSource
import com.example.androidpaging.data.Repo
import com.example.androidpaging.data.RepoDao
import java.util.concurrent.Executor

class GithubLocalCache (
    private val repoDao: RepoDao
) {
    suspend fun insert(repos: List<Repo>, insertFinished: () -> Unit) {
        repoDao.insertAll(repos)
    }

    fun get(name: String): DataSource.Factory<Int,Repo>{
        return repoDao.get(name = name)
    }
}