package com.example.androidpaging

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.androidpaging.data.GithubRepository


class MainViewModel(repository: GithubRepository) : ViewModel() {
    // TODO: Implement the ViewModel
}

class  MainViewModelFactory  internal constructor (
private val repository: GithubRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(repository) as T
    }
}