package com.example.androidpaging.data





import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RepoDao {
    @Query("SELECT * from repo_table ORDER BY name ASC")
    fun getAll() : LiveData<List<Repo>>

    @Query("SELECT * FROM repos WHERE (name LIKE :name) OR (description LIKE " +
            ":name) ORDER BY stars DESC, name ASC")
    fun get(name: String): DataSource.Factory<Int,Repo>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(repo: Repo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrReplaceIfNeed(repo: Repo)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(repo: List<Repo>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllOrReplaceAllIfNeed(repo: List<Repo>)

    @Query ("DELETE FROM repo_table WHERE id = :id")
    suspend fun delete(id: Long)

    @Query("DELETE FROM repo_table")
    suspend fun deleteAll()


}
