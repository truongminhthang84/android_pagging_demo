package com.example.androidpaging.data


import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import com.example.androidpaging.api.GithubService
import com.example.androidpaging.db.GithubLocalCache

class GithubRepository(
    private val service: GithubService,
    private val cache: GithubLocalCache
) {

    fun search(query: String): RepoSearchResult {
        val dataSourceFactory = cache.get(name = query)
        val boundaryCallback = RepoBoundaryCallback(query = query, service = service, cache = cache)
        val networkErrors = boundaryCallback.networkErrors
        var data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()
        return RepoSearchResult(data = data, networkErrors = networkErrors)
    }


    companion object {
        private const val DATABASE_PAGE_SIZE = 20
        // For Singleton instantiation
        @Volatile
        private var instance: GithubRepository? = null

        fun getInstance(service: GithubService, cache: GithubLocalCache) =
            instance ?: synchronized(this) {
                instance ?: GithubRepository(service, cache).also { instance = it }
            }
    }
}