package com.example.androidpaging.data


import com.google.gson.annotations.SerializedName



import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey

@Parcelize
@Entity(tableName = "repo_table")
data class Repo(@PrimaryKey @field:SerializedName("id") val id: Long,
                @field:SerializedName("name") val name: String,
                @field:SerializedName("full_name") val fullName: String,
                @field:SerializedName("description") val description: String?,
                @field:SerializedName("html_url") val url: String,
                @field:SerializedName("stargazers_count") val stars: Int,
                @field:SerializedName("forks_count") val forks: Int,
                @field:SerializedName("language") val language: String?): Parcelable