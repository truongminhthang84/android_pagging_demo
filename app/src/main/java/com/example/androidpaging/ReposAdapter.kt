package com.example.androidpaging


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.androidpaging.data.Repo

/**
 * Adapter for the [RecyclerView] in [MainActivity].
 */
class ReposAdapter : ListAdapter<Repos, RecyclerView.ViewHolder>(ReposDiffCallback()) {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val repo = getItem(position)
        (holder as ReposViewHolder).bind(repo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ReposViewHolder(RepoItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false))
    }

    class ReposViewHolder(
        private val binding: RepoItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setOnClickListener {
                binding.repo?.let { repo ->
                    navigateToPlant(repo, it)
                }
            }
        }

        private fun navigateToPlant(
                repo: Repos,
            it: View
        ) {
            val direction = HomeViewPagerFragmentDirections.actionViewPagerFragmentToPlantDetailFragment(repo.id  )
            it.findNavController().navigate(direction)
        }

        fun bind(item: Repos) {
            binding.apply {
                repo = item
                executePendingBindings()
            }
        }
    }
}

private class ReposDiffCallback : DiffUtil.ItemCallback<Repo>() {

    override fun areItemsTheSame(oldItem: Repo, newItem: Repo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Repo, newItem: Repo): Boolean {
        return oldItem == newItem
    }
}