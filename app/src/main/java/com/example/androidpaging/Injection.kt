package com.example.androidpaging

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.example.androidpaging.api.GithubService
import com.example.androidpaging.data.AppDatabase
import com.example.androidpaging.data.GithubRepository
import com.example.androidpaging.db.GithubLocalCache

object Injection {

    /**
     * Creates an instance of [GithubLocalCache] based on the database DAO.
     */
    private fun provideCache(context: Context): GithubLocalCache {
        val database = AppDatabase.getInstance(context)
        return GithubLocalCache(database.repoDao())
    }

    /**
     * Creates an instance of [GithubRepository] based on the [GithubService] and a
     * [GithubLocalCache]
     */
    private fun provideGithubRepository(context: Context): GithubRepository {
        return GithubRepository(GithubService.create(), provideCache(context))
    }

    /**
     * Provides the [ViewModelProvider.Factory] that is then used to get a reference to
     * [ViewModel] objects.
     */
    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return MainViewModelFactory(provideGithubRepository(context))
    }
}